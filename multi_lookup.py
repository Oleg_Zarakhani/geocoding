

from __future__ import division



import pandas as pd
import numpy as np
from geopy import geocoders
from geopy.distance import vincenty
import time
from ast import literal_eval

from itertools import combinations



# pd.set_option('display.height', 1000)
# pd.set_option('display.max_rows', 500)
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)




# pd.read_csv('')

# #for i in combinations(services.keys(),2):

# data = pd.read_csv('Veyo Prov&Prof_2016-09-30-10-47-48.csv')


# address_list = [create_address_string(a) for i,a in data.iterrows()]

# data = pd.read_csv('geocoded.csv')

# for s in services.keys():

# 	# new_vec = []
# 	# for i in range(len(data[s])):
# 	# 	new_vec.append(literal_eval(data[s][i])[::-1])

# 	data[s] = [literal_eval(i) for i in data[s]]



# g = geocoders.GoogleV3(client_id = 'gme-2pointbllc', secret_key = 'R3AaMbv1kYG0P1DLu_Fldm7VKZ8=')
# b = geocoders.Bing('AvBoxsI3xM2Rk5RRotwJQ12zJuWdgX2b3Xi8awQdqmb3jMJZTffGx1txRRcJPgKy')
# a = geocoders.ArcGIS()
# n = geocoders.Nominatim()



# services = {'google':g, 
# 			'bing': b,
# 			'ArcGIS': a,
# 			'Nominatim': n}






def lat_long_tup(address, service, service_name):

	LL_dict = {}
	try:
		call = service.geocode(address)
		call_LL = (call.latitude, call.longitude)
	except:
		call_LL = (0, 0)

	LL_dict[service_name] = call_LL

	return LL_dict







def calculate_inter_distances(row, services):

	non_zeros = []
	for k in services.keys():
		if (np.mean(row[k]) != 0) or (row[k] is None):
			non_zeros.append(k)

	dist_dict = dict()
	for c in combinations(non_zeros, 2):
		
		#print c
		d_dict = dict()
		dist = vincenty(row[c[0]], row[c[1]]).meters

		d_dict[c] = dist
		dist_dict = dict(dist_dict.items() + d_dict.items())


	return dist_dict 


def find_max_min(dist_dict):
	inverse = [(value, key) for key, value in dist_dict.items()]
	minimum = min(inverse)
	maximum = max(inverse)

	return minimum, maximumm




def find_winners(dist_dict, thresh):

	candidates = [list(k) for k,v in dist_dict.iteritems() if v <= thresh]
	if len(candidates) == 0:
		return [0], 0
	uniques = list(np.unique([item for sublist in candidates for item in sublist]))
	winner = list([k for k,v in dist_dict.iteritems() if v == min(dist_dict.values()) and v <= thresh][0])[np.random.randint(2)]

	return uniques, winner




def winner_and_candidates(dist_dict, lat_lon_dict, thresh = 100):

	
	#print dist_dict
	uniques, winner = find_winners(dist_dict, thresh)

	selection_results = dict()
	selection_results['candidates'] = uniques
	selection_results['winner'] = winner
	if winner == 0:
		selection_results['lat_lon'] = (0, 0)
	else: selection_results['lat_lon'] = lat_lon_dict[winner]

	return selection_results





def find_full_call_data(address, services):
	lookup_dat = dict()
	for k,v in services.iteritems():
		try:
			lookup_dat[k] = v.geocode(address)
			
		except:	
			lookup_dat[k] = []
	
	return lookup_dat

def extract_lat_lon(lookup_data):

	lat_lon_dict = dict()
	for k,v in lookup_data.iteritems():
		if (lookup_data[k] is None) or (len(lookup_data[k]) == 0):
			lat_lon_dict[k] = (0,0)

		else: lat_lon_dict[k] = (lookup_data[k].latitude, lookup_data[k].longitude)

	return lat_lon_dict



# def lat_long_tup(address, service, lookup_data, service_name):

# 	LL_dict = {}
# 	try:
# 		call = service.geocode(address)
# 		call_LL = (call.latitude, call.longitude)
# 	except:
# 		continue

# 	LL_dict[service_name] = call_LL

# 	return LL_dict


def find_accurate_string(selection, lookup_data, address):
	
	if selection['winner'] == 0:
		return {'winner_string': '', 'standard_form': ''}
	
	# if selection['winner'] == 'Nominatim':
	# 	arc_call = a.geocode(address)
	# 	arcGIS_str = str(arc_call.address)
	# 	return {'winner_string': arcGIS_str, 'standard_form': arcGIS_str}



	winner_str = str(lookup_data[selection['winner']].address)
	#winner_str = str(winner_call.address)
	#if selection['winner'] == 'ArcGIS'

	try:
		arc_call = a.geocode(address)
		arcGIS_str = arc_call.address
	except:
		#if (address in lookup_data)
		if len(lookup_data['ArcGIS']) == 0: arcGIS_str = str('')
		else: arcGIS_str = str(lookup_data['ArcGIS'].address)
	
	# arc_call = a.geocode(winner_str)
	# if arc_call is None: arcGIS_str = str(lookup_data['ArcGIS'].address)
	# else: arcGIS_str = str(arc_call.address)

	return {'winner_string': winner_str, 'standard_form': arcGIS_str}



def find_accurate_string(selection, lookup_data, address):
	
	if selection['winner'] == 0:
		return {'winner_string': '', 'standard_form': ''}
	


	winner_str = str(lookup_data[selection['winner']].address)


	try:
		if selection['winner'] == 'google': formatted_address = str(lookup_data[selection['winner']].raw['formatted_address'])
		if selection['winner'] == 'bing': formatted_address = str(lookup_data[selection['winner']].raw['address']['formattedAddress'])
		if selection['winner'] == 'ArcGIS': formatted_address = str(lookup_data[selection['winner']].raw['name'])
		if selection['winner'] == 'Nominatim': formatted_address = str(lookup_data[selection['winner']].raw['display_name'])
		
	except:
	
		formatted_address = ''
	

	return {'winner_string': winner_str, 'standard_form': formatted_address}













def geocode_data(data, services):

	new_data = pd.DataFrame()

	for index, row in data.iterrows():

		print index
		#address = create_address_string(row)
		#address = row['OriginFullAddress (Trip Address)']
		address = ' '.join(row.dropna().values)
		#name = row['OriginFullAddress (Trip Address)']
		lookups = dict()
		lookups['Address'] = address
		#lookups['Name'] = name

		lookup_data = find_full_call_data(address, services)
		lat_lon_dict = extract_lat_lon(lookup_data)

		dist_dict = calculate_inter_distances(lat_lon_dict, services)
		print index 
		selection = winner_and_candidates(dist_dict, lat_lon_dict, thresh = 100)
		print selection['winner']
		strings = find_accurate_string(selection, lookup_data, address)


		lat_lon_dict.update(selection)
		lat_lon_dict.update(strings)
		lat_lon_dict['Address'] = address
		to_add = pd.Series(lat_lon_dict)
		
		print to_add
		new_data = new_data.append(to_add, ignore_index = True)



	cols = ['Address', 'standard_form', 'winner', 'candidates', 'lat_lon', 'ArcGIS', 'Nominatim', 'bing', 'google', 'winner_string']
	new_data = new_data[cols]
	new_data.to_csv('tripaddress.csv', index = False)
	out = pd.concat([data, new_data], axis = 1)
	out.to_csv('eligibility_michigan.csv', index = False)		

		# for service_name, service in services.iteritems():
		# 	LL_dict = lat_long_tup(address, service, service_name)
		# 	lookups = dict(lookups.items() + LL_dict.items())



	



def geocode_multiple(data):

	new_data = pd.DataFrame()

	for index, row in data.iterrows():

		print index
		address = create_address_string(row)


		name = row['LAST NAME']

		lookups = dict()
		lookups['Address'] = address
		lookups['Name'] = name

		for service_name, service in services.iteritems():
			LL_dict = lat_long_tup(address, service, service_name)
			lookups = dict(lookups.items() + LL_dict.items())


		selection = winner_and_candidates(row, services, thresh = 100)
		lookups.update(selection)

		to_add = pd.Series(lookups)
		
		print to_add
		new_data = new_data.append(lookups, ignore_index = True)

	return new_data


def add_winner_to_data(data):

	new_data = pd.DataFrame()
	for index, row in data.iterrows():
		#print(row)
		dist_dict = calculate_inter_distances(row, services)
		print index 
		selection = winner_and_candidates(dist_dict, services, thresh = 100)
		print selection['winner']
		strings = find_accurate_string(selection, services, row.Address)
		selection.update(strings)
		new_data = new_data.append(selection, ignore_index = True)

	d = candidate_occurance(new_data)
	out = pd.concat([data, new_data], axis = 1)


def candidate_occurance(data):

	candies = data.candidates
	d = {}
	for s in services.keys():
		s_count = [1 for k in candies if s in k]
		s_percent = sum(s_count)/data.shape[0]
		
		d[s] = s_percent
	print d
	return d






def create_address_string(row):

	street = row.ADDRESS1
	city = row.CITY
	state = row.ST
	zip_code = row.ZIP
	address = "%s, %s, %s, %s" % (street, city, state, zip_code)

	return address






























