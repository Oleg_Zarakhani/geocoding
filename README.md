Geocoding script that hits multiple API's


main function of the script is geocode_data in multi_lookup

The basics of the geocode_data function as follow:

Loop over each row of dataset in csv format which contains an adress string.
For each row the 'address' variable is the string to be geocoded by the algorithm.
find_full_call_data hits every api in the list.
lat_lon_dict packages in a friendly format for analysis
calculate_inter_dists determines which services are closest to each other.
winner_and candidates determines which is the winning string


the process is saved in a json and gets unrapped and appended to the original string then saved as CSV

Process is executed by running lookup_exec.py